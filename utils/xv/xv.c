#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include "../../shared/colors.h"

#define ESCC(c) (isprint(c) ? c : '.')
#define put(str) fputs((str), stdout)
#define DATA_SIZE 16
#define COLORS (char*[]){C_RED, C_GREEN, C_BLUE, C_CYAN, C_PINK, C_PURPLE}
#define COLORS_SIZE 6

char *get_color(size_t i) {
	return COLORS[i % COLORS_SIZE];
}

void show_data(unsigned char *data, int dsize, long addr) {
	assert(dsize <= DATA_SIZE);

	char cdata[DATA_SIZE] = {0};

	for (size_t i = 0; i < DATA_SIZE; i++) {
		if (i >= dsize) break;
		cdata[i] = ESCC(data[i]);
	}

	if (C_ENABLED) {
		printf(C_RESET C_WHITE C_BOLD "%08lX", addr);
		put(C_RESET " |");

		for (size_t i = 0; i < DATA_SIZE; i++) {
			if (i % 8 == 0 && i != 0) put("  ");
			if (i >= dsize) { put("   "); continue; }
			put(get_color(i));
			put(C_BOLD);
			printf(" %02X", data[i]);
		}

		put(C_RESET " | ");

		for (size_t i = 0; i < DATA_SIZE; i++) {
			if (i % 8 == 0 && i != 0) put("  ");
			put(get_color(i));
			put(C_BOLD);
			printf("%c", cdata[i]);
		}
		put(C_RESET " |\n");
	} else {
		printf("%08lX", addr);
		put(" |");

		for (size_t i = 0; i < DATA_SIZE; i++) {
			if (i % 8 == 0 && i != 0) put("  ");
			if (i >= dsize) { put("   "); continue; }
			printf(" %02X", data[i]);
		}

		put(" | ");

		for (size_t i = 0; i < DATA_SIZE; i++) {
			if (i % 8 == 0 && i != 0) put("  ");
			printf("%c", cdata[i]);
		}
		put(" |\n");
	}
}

int main(int argc, char **argv) {
	C_init();

	FILE *file;
	switch (argc-1) {
		case 0:
			file = stdin;
			break;
		case 1:
			errno = 0;
			file = fopen(argv[1], "rb");
			if (file == NULL) {perror("xv from UPRP, fatal error while reading file"); return 1;}
			break;
		default:
			fputs("xv from UPRP, usage: xv [file]\nSee a pretty printed hex dump of a file or of standard input\n", stderr);
			return 1;
			break;
	}

	int ch = getc(file);
	unsigned char str[DATA_SIZE + 1] = {0};
	long addr = 0;
	size_t str_size = 0;

	for (str_size = 0; ch != EOF; ch = getc(file), str_size++, addr++) {
		if (ferror(file)) {
			perror("xv, error while reading stream");
		}

		if (str_size >= DATA_SIZE) {
			show_data(str, str_size, addr);
			str_size = 0;
		}

		str[str_size] = (unsigned char)ch;
	}
	str[str_size+1] = '\0';
	show_data(str, str_size, addr);
}
