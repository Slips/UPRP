// IO and checking
#include <stdio.h>
#include <string.h>

// Memory management
#include <stdlib.h>

// User and group handling
#include <pwd.h>
#include <sys/wait.h>
#include <unistd.h>
#include <shadow.h>

// Signal handling
#include <signal.h>

// Terminal handling
#include "../../shared/term.h"
#include "../../shared/colors.h"

#define PASSWORD_SIZE 1024

FILE *tty;
struct termios term;

void signal_handler(int sig) {
	Term_AddFlag(term, ECHO);
	Term_RemoveFlag(term, ICANON | ECHOE | ECHOK );
	Term_SetState(term, tty);
	fclose(tty);

	if (C_ENABLED) {
		fputs("\n" C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Interrupted authentication\n" C_RESET, stderr);
	} else {
		fputs("\n[Boas] Interrupted authentication\n", stderr);
	}
	
	exit(1);
}

static void set_perms(uid_t uid, gid_t gid) {
	setgid(gid);
	setegid(gid);
	setuid(uid);
	seteuid(uid);
}

static void err_handler(uid_t set_uid, gid_t set_gid) {
	set_perms(set_uid, set_gid);
	perror("[Boas] Fatal error");
}

int main(int argc, char **argv) {
	C_init();

	if (geteuid() != 0 || getegid() != 0) {
		if (C_ENABLED) {
			fputs(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: incorrect permissions, run 'make setuid' as root inside the Boas source to set correct permissions\n" C_RESET, stderr);
		} else {
			fputs("[Boas] Fatal error: incorrect permissions, run 'make setuid' as root inside the Boas source to set correct permissions\n", stderr);
		}
		return 1;
	}

	uid_t default_uid = getuid();
	gid_t default_gid = getgid();

	struct passwd *runner;
	struct spwd *runner_shadow;

	runner = getpwuid(getuid());
	runner_shadow = getspnam(runner->pw_name);

	signal(SIGINT, signal_handler);

	if (getuid() != 0 && getgid() != 0) {
		int correct = 0;
		for (int attempts = 0; attempts < 3; attempts++) {
			if (C_ENABLED) {
				fprintf(stderr, C_RESET C_BOLD C_BLUE "[Boas]" C_RESET C_BOLD " Password for '%s': " C_RESET, runner->pw_name);
			} else {
				fprintf(stderr, "[Boas] Password for '%s': ", runner->pw_name);
			}
			// Setup getting the password
			tty = fopen("/dev/tty", "r");
			Term_GetState(term, tty);
			Term_RemoveFlag(term, ECHO);
			Term_AddFlag(term, ICANON | ECHOE | ECHOK);
			Term_SetState(term, tty);

			char input[PASSWORD_SIZE] = {0};
			size_t size = 0;

			for (int c = getc(tty); c != EOF && c != '\n'; c = getc(tty)) {
				if (size >= PASSWORD_SIZE) break;
				input[size] = (char)c;
				size++;
			}

			// Clean up
			Term_AddFlag(term, ECHO);
			Term_RemoveFlag(term, ICANON | ECHOE | ECHOK );
			Term_SetState(term, tty);
			fclose(tty);

			if (*input == '\0') {
				if (C_ENABLED) {
					fputs(C_RESET C_BOLD C_RED "\n[Boas]" C_RESET C_BOLD " A password is required\n" C_RESET, stderr);
				} else {
					fputs("\n[Boas] A password is required\n", stderr);
				}
				correct = 0;
				continue;
			}

			putchar('\n');

			correct = strcmp(runner_shadow->sp_pwdp, crypt(input, runner_shadow->sp_pwdp)) == 0;

			(void) memset(input, 0, PASSWORD_SIZE);

			if (!correct) {
				if (C_ENABLED) {
					fputs(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Incorrect password\n" C_RESET, stderr);
				} else {
					fputs("[Boas] Incorrect password\n", stderr);
				}
			} else {
				break;
			}
		}

		if (!correct) {
			if (C_ENABLED) {
				fputs(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Failed to authenticate with password\n" C_RESET, stderr);
			} else {
				fputs("[Boas] Failed to authenticate with password\n", stderr);
			}
			return 1;
		}
	}

	signal(SIGINT, SIG_DFL);

	char **ex = calloc(argc+1, sizeof(char*));
	
	if (argc == 1) {
		ex[0] = getpwuid(0)->pw_shell;
	}

	for (int i = 1; i < argc; i++) {
		ex[i-1] = argv[i];
	}

	set_perms(0, 0);

	pid_t pid = fork();

	switch (pid) {
		case -1: // Error
			free(ex);
			err_handler(default_uid, default_gid);
			break;
		case 0: // Child process
			execvp(ex[0], ex);
			break;
		default: { // Parent process
			set_perms(default_uid, default_gid);
			pid_t status;
			pid_t finished = wait(&status);

			free(ex);

			if (finished != pid) {
				set_perms(default_uid, default_gid);
				if (C_ENABLED) {
					fputs(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Got PID of unknown process, expected PID of child\n" C_RESET, stderr);
				} else {
					fputs("[Boas] Fatal error: Got PID of unknown process, expected PID of child\n", stderr);
				}
				return 1;
			}

			if (WIFEXITED(status)) {
				set_perms(default_uid, default_gid);
				return WEXITSTATUS(status);
			}

			if (WIFSIGNALED(status)) {
				set_perms(default_uid, default_gid);
				return WTERMSIG(status);
			}

			return status;
		}
	}
}
