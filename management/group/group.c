#include <unistd.h>
#include <grp.h>
#include <stdio.h>
#include <errno.h>
#include "../../shared/colors.h"

#define eputs(err) if (C_ENABLED) { fputs(C_RESET C_BOLD C_RED err "\n", stderr); } else { fputs(err"\n", stderr); }

static void print_ginfo(struct group *grp) {
	if (C_ENABLED) {
		printf(
				C_RESET"Group: "C_BOLD C_BLUE"%s\n"
				C_RESET"GID: "C_BOLD C_BLUE"%d\n"
				C_RESET"Members:"C_BOLD C_BLUE"\n",
				grp->gr_name, grp->gr_gid
			);
	} else {
		printf(
				"Group: %s\n"
				"GID: %d\n"
				"Members:\n",
				grp->gr_name, grp->gr_gid
			);
	}

	for (size_t i = 0; grp->gr_mem[i] != NULL; i++) {
		puts(grp->gr_mem[i]);
	}
}

static void handle_err(int err, char *username) {
	if (C_ENABLED) {
		switch (err) {
			case EIO:
				fprintf(stderr, C_RESET C_BOLD C_RED "Could not get group '%s' information: IO error\n", username);
				break;
			case ENOMEM:
				fprintf(stderr, C_RESET C_BOLD C_RED "Could not get group '%s' information: Out of memory\n", username);
				break;
			default:
				fprintf(stderr, C_RESET C_BOLD C_RED "Could not get group '%s' information: Not found\n", username);
				break;
		}
	} else {
		switch (err) {
			case EIO:
				fprintf(stderr, "Could not get user '%s' information: IO error\n", username);
				break;
			case ENOMEM:
				fprintf(stderr, "Could not get user '%s' information: Out of memory\n", username);
				break;
			default:
				fprintf(stderr, "Could not get user '%s' information: Not found\n", username);
				break;
		}
	}
}

int main(int argc, char **argv) {
	struct group *grp;
	C_init();

	switch (argc-1) {
		case 0:
			errno = 0;
			grp = getgrgid(getgid());
			if (grp == NULL) {
				handle_err(errno, "default group");
				return 1;
			}
			print_ginfo(grp);
			break;
		case 1:
			grp = getgrnam(argv[1]);
			if (grp == NULL) {
				handle_err(errno, argv[1]);
				return 1;
			}
			print_ginfo(grp);
			break;
		default:
			for (int i = 1; i < argc; i++) {
				grp = getgrnam(argv[i]);
				if (grp == NULL) {
					handle_err(errno, argv[i]);
					return 1;
				}
				print_ginfo(grp);
				if (i != argc-1) {
					printf("\n");
				}
			}
	}
	return 0;
}
