# UPRP

**U**serspace **P**rograms **R**eplacement **P**roject

This project aims to replace most programs that I (Bowuigi) use with other programs, with a different design targeted towards simplicity and ease of use.

# Currently done:

- Shared/colors (library): Handle ANSI escape secuences and choose when to use them
- Management/user (program): Get information about an user or a list of them (needs manpage)
- Management/group (program): Get information about a group or a list of them (needs manpage)
- Utils/xv (program): View files of any type in hexadecimal (needs manpage)
- Management/boas (program): Execute programs as root

# In progress

- Management/mop (program): Manage packages with it or with the exposed directory interface

# Programs that will most likely be here:

- Text manipulation programs using the capabilities of the shell and some form of structural Regex (My pattern matching library, Match, could help)

- Simple file management programs (like the usual `ls`, `cp`, `mv`, `rm`)

- Process monitoring (`top`, `ps`, `kill`, `pgrep`, `pkill`, `killall`)

- A text editor with syntax highlighting using mainly structural Regex

- A shell (Very simple, pipes and redirections only)
 
- System management utilities (hex viewer, socket inspector)
 
- Service manager (Like Runit)
 
- Privilege escalation

- Login manager

- A diff that uses structural Regex to show what part of a file changed, and not just the lines, like Rob Pike proposed
 
- Utilities for shell scripting (`test`, a testing framework)
 
- Utilities for planning the future (`cal`, `date`, something to combine both with an agenda)
 
- Extras (JSON conversion, a syntax highlighter, web browser, a cooler `who`)
 
- Games (Roguelikes, incrementals, or something like that)

Everything should work on the TTY

The programs should be written in POSIX C99
